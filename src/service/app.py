from fastapi import FastAPI
import mlflow
import uvicorn
from contextlib import asynccontextmanager
from src.service.config import Config
from prometheus_fastapi_instrumentator import Instrumentator
from schemas import PredictionResponse, PredictionFeatures

config = Config()
model = mlflow.pyfunc.load_model(config.get_model_uri())


@asynccontextmanager
async def startup(app: FastAPI):
    instrumentator.expose(app)
    yield


app = FastAPI(lifespan=startup)
instrumentator = Instrumentator().instrument(app)


@app.post("/predict")
async def predict(request: PredictionFeatures):
    return PredictionResponse(prediction=model.predict([request.features]))


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=config.port)
