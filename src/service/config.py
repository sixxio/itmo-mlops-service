import mlflow


class Config:
    model_name = 'gradient-boosting-model'
    mlflow_tracking_uri = 'http://192.168.3.3:10003'
    port = 8000

    def get_model_uri(self):
        mlflow.set_tracking_uri(self.mlflow_tracking_uri)
        client = mlflow.MlflowClient(self.mlflow_tracking_uri)
        model_versions = client.search_model_versions(f"name='{self.model_name}'")
        return f"models:/{self.model_name}/{max([i.version for i in model_versions])}"
