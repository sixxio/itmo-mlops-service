from pydantic import BaseModel, Field
from typing import List, Optional


class PredictionFeatures(BaseModel):
    features: Optional[List[bool]] = Field(..., description="List of features", example=[1, 0, 1])


class PredictionResponse(BaseModel):
    prediction: bool = Field(..., description="Prediction: 1 - malware detected, 0 - application is safe.")
