FROM python:3.10-slim
WORKDIR /
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY src/service /src
CMD ["python", "src/app.py"]
